## Upwork Challenge Task - Gowtham

### Installation
1) Run **cp .env.example .env**
2) Run **php artisan key:generate**
3) Run **composer install**
4) Configure the database credentials and run **php artisan migrate**

The application will be ready

### Images
![GUI 1](Images/Page 1.png)
![GUI 2](Images/Page 2.png)
![GUI 3](Images/Page 3.png)
![GUI 4](Images/Page 4.png)
![GUI 5](Images/Page 5.png)


