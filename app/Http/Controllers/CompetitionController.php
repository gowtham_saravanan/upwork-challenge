<?php

namespace App\Http\Controllers;

use App\Http\Requests\StartCompetitionRequest;
use App\Models\Competition;
use App\Models\Contestant;
use App\Models\Round;
use App\Models\RoundAllotment;
use App\Models\WaitingAllotment;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CompetitionController extends Controller
{
    /**
     * Home page.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function start()
    {
        try {
            return view('start');
        } catch (\Exception $exception) {
            Log::error('Some error in start page', $exception->getTrace());
            abort(500);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function startCompetition(StartCompetitionRequest $request)
    {
        try {
            // Clear Previous Data
            $this->clearAllData();


            $noOfRounds = $request->input('no_of_rounds');
            // Create competition
            Competition::create(['no_of_rounds' => $noOfRounds]);

            // Create available rounds
            $rounds = [];
            for ($i = 1; $i <= $noOfRounds; $i++) {
                $rounds[] = ['no' => $i, 'created_at' => now(), 'updated_at' => now()];
            }
            Round::insert($rounds);


            // Create Contestants
            $data = [];
            foreach ($request->input('users') as $userName) {
                $data[] = [
                    'name' => $userName,
                    'created_at' => now(),
                    'updated_at' => now()
                ];
            }
            Contestant::insert($data);
            $contestants = Contestant::all();

            $totalNoOfUsers = count($data);


            // Logic if total no of users and no of rounds are divisible
            if (($totalNoOfUsers % $noOfRounds == 0)) {
                $roundAllotment = [];
                $groupCount = $totalNoOfUsers / $noOfRounds;
                $loopCount = 1;
                $groupNo = 1;
                foreach ($contestants as $contestant) {
                    $result = $loopCount / $groupCount;

                    $roundAllotment[] = [
                        'contestant_id' => $contestant->id,
                        'round_id' => 1,
                        'group_no' => $groupNo,
                        'created_at' => now(),
                        'updated_at' => now()
                    ];
                    $loopCount++;
                    if (ceil($result) == floor($result)) {
                        $groupNo++;
                    }
                }
                RoundAllotment::insert($roundAllotment);
            } else {
                // Logic if total no of users and no of rounds are divisible

                $roundAllotment = [];
                $waitingAllotment = [];
                $groupCount = floor($totalNoOfUsers / $noOfRounds);
                $groups = [];

                for ($i = 1; $i <= $noOfRounds - 1; $i++) {
                    $groups[] = $groupCount;
                }

                $groups[] = $totalNoOfUsers - array_sum($groups);
                rsort($groups);

                $loopCount = 0;
                foreach ($groups as $group) {
                    if ($group == 1) {
                        $contestant = $contestants[$loopCount];
                        $waitingAllotment[] = [
                            'contestant_id' => $contestant->id,
                            'round_id' => 1,
                            'created_at' => now(),
                            'updated_at' => now()
                        ];
                        $loopCount++;

                    } else {
                        for ($i = 1; $i <= $group; $i++) {
                            $contestant = $contestants[$loopCount];
                            $roundAllotment[] = [
                                'contestant_id' => $contestant->id,
                                'round_id' => 1,
                                'group_no' => $group,
                                'created_at' => now(),
                                'updated_at' => now()
                            ];
                            $loopCount++;
                        }
                    }
                }

                // Insert into round and waiting list
                RoundAllotment::insert($roundAllotment);
                WaitingAllotment::insert($waitingAllotment);
            }

            return redirect()->route('round.details', 1);

        } catch (\Exception $exception) {
            Log::error('Some error in start competition', $exception->getTrace());
            abort(500);
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function roundDetails($id)
    {
        try {
            $round = Round::findOrFail($id);
            $round->load('contestants');

            $contestants = $round->contestants->groupBy('pivot.group_no');
            $waitingContestants = Contestant::whereIn('id',
                WaitingAllotment::pluck('contestant_id')->toArray())->get();

            return view('round-details', compact('round', 'contestants', 'waitingContestants'));
        } catch (ModelNotFoundException $exception) {
            abort(404);
        } catch (\Exception $exception) {
            Log::error('Some error in round details page', $exception->getTrace());
            abort(500);
        }

    }

    public function completeRound($id)
    {
        try {
            $competition = Competition::first();

            $round = Round::findOrFail($id);
            $round->load('contestants');

            $groupedContestants = $round->contestants->groupBy('pivot.group_no');
            $selectedContestants = [];

            foreach ($groupedContestants as $group => $contestants) {
                $selectedIndex = rand(1, $contestants->count()) - 1;
                $selectedContestants [] = $contestants[$selectedIndex];
            }

            // Logic if only one selected contestant available
            if (count($selectedContestants) == 1) {

                // if no waiting contestants are there, declare as winner
                if (WaitingAllotment::count() == 0) {
                    $competition->winner_id = $selectedContestants[0]->id;
                    $competition->save();
                    return redirect()->route('winner');
                } else {
                    //  If waiting contestants are available, move them to next round one by one
                    $waitingContestant = WaitingAllotment::first();
                    RoundAllotment::insert([
                        [
                            'contestant_id' => $selectedContestants[0]->id,
                            'round_id' => $round->no + 1,
                            'group_no' => 1,
                            'created_at' => now(),
                            'updated_at' => now()
                        ],
                        [
                            'contestant_id' => $waitingContestant->contestant_id,
                            'round_id' => $round->no + 1,
                            'group_no' => 1,
                            'created_at' => now(),
                            'updated_at' => now()
                        ]
                    ]);
                    $waitingContestant->delete();
                }
            }
            // Logic if more selected contestant available

            if (count($selectedContestants) > 1) {
                RoundAllotment::insert([
                    [
                        'contestant_id' => $selectedContestants[0]->id,
                        'round_id' => $round->no + 1,
                        'group_no' => 1,
                        'created_at' => now(),
                        'updated_at' => now()
                    ],
                    [
                        'contestant_id' => $selectedContestants[1]->id,
                        'round_id' => $round->no + 1,
                        'group_no' => 1,
                        'created_at' => now(),
                        'updated_at' => now()
                    ]
                ]);
                unset($selectedContestants[0]);
                unset($selectedContestants[1]);
                $waitingContestants = [];

                foreach ($selectedContestants as $contestant) {
                    $waitingContestants[] = [
                        'contestant_id' => $contestant->id,
                        'round_id' => $round->no + 1,
                        'created_at' => now(),
                        'updated_at' => now()
                    ];
                }

                WaitingAllotment::insert($waitingContestants);
            }

            return redirect()->route('round.details', $round->no + 1);
        } catch (ModelNotFoundException $exception) {
            abort(404);
        } catch (\Exception $exception) {
            Log::error('Some error in start page', $exception->getTrace());
            abort(500);
        }

    }

    public function winner()
    {
        try {
            $winner = Contestant::find(Competition::first()->winner_id)->name;
            return view('winner', compact('winner'));
        } catch (\Exception $exception) {
            abort(500);
        }
    }

    public function clearAllData()
    {
        Competition::truncate();
        Contestant::truncate();
        RoundAllotment::truncate();
        WaitingAllotment::truncate();
        Round::truncate();
    }

}
