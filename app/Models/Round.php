<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Round extends Model
{
    protected $fillable = [
        'no'
    ];

    public function contestants(){
        return $this->belongsToMany(Contestant::class, 'round_allotments', 'round_id', 'contestant_id')->withPivot(['group_no']);
    }

    public function waitingContestants(){
        return $this->belongsToMany(Contestant::class, 'waiting_allotments', 'round_id', 'contestant_id');
    }

    use HasFactory;
}
