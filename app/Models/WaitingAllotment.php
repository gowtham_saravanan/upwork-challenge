<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WaitingAllotment extends Model
{
    protected $fillable = [
        'contestant_id',
        'round_id'
    ];

    public function contestant(){
        $this->belongsTo(Contestant::class);
    }
    use HasFactory;
}
