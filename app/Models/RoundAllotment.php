<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RoundAllotment extends Model
{
    protected $fillable = [
        'contestant_id',
        'round_id',
        'group_no'
    ];

    use HasFactory;
}
