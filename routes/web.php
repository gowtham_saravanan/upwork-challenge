<?php

use App\Http\Controllers\CompetitionController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [CompetitionController::class, 'start'])->name('home');
Route::post('/start-competition', [CompetitionController::class, 'startCompetition'])->name('start.competition');

Route::get('/round/{id}/participants', [CompetitionController::class, 'roundDetails'])->name('round.details');
Route::get('/round/{id}/complete', [CompetitionController::class, 'completeRound'])->name('round.complete');

Route::get('/winner', [CompetitionController::class, 'winner'])->name('winner');
