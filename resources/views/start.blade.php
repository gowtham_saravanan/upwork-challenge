@extends('layouts.app')

@section('content')
    <div class="d-flex justify-content-center align-items-center h-100 bg-light">
        <div class="p-5 card text-center" style="width: 600px">
            <label class="mb-4"><h2> Upwork Challenge for Full Stack Developer - Gowtham S </h2></label>

            <form id="competition" method="post" action="{{ route('start.competition') }}">
                {{ csrf_field() }}
                <div class="form-group text-left">
                    <label for="users">Enter the name of the competitors</label>
                    <select class="form-control" id="users" name="users[]" multiple></select>
                    <small id="users" class="form-text text-muted">Enter a name and press enter button. Follow the same
                        procedure to many many names.</small>
                    @if ($errors->has('users'))
                        <span class="text-danger">{{ $errors->first('users') }}</span>
                    @endif
                </div>
                <div class="form-group text-left">
                    <label for="no_of_rounds">Enter no of rounds</label>
                    <input class="form-control" id="no_of_rounds" name="no_of_rounds" type="number">
                    @if ($errors->has('no_of_rounds'))
                        <span class="text-danger">{{ $errors->first('no_of_rounds') }}</span>
                    @endif
                </div>
                <div class="form-group">
                    <button class="btn btn-success form-control" id="submit" name="submit"> Submit</button>
                </div>
            </form>
        </div>
    </div>

@endsection

@push('scripts')
    <script>
        $(document).ready(function () {
            $('#users').select2({
                tags: true,
                width: '100%'
            });
        });
    </script>
@endpush
