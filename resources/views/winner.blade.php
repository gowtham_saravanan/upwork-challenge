@extends('layouts.app')

@section('content')
    <div class=" d-flex justify-content-center align-items-center flex-column h-100 bg-light">
        <div class="p-5 card shadow text-center">
            <div class="py-3">
                <h2>Competition ended</h2>
            </div>

            <div class="py-3">
                <p>The winner is <strong><u> {{ $winner }} </u></strong>.</p>
            </div>


            <div class="py-3">
                <a href="{{ route('home') }}">Start again</a>
            </div>
        </div>
    </div>
@endsection

