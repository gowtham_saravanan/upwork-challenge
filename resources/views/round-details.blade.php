@extends('layouts.app')

@section('content')
    <div class="h-100 mx-auto bg-light">
        <div class="p-5 text-center row justify-content-start mx-auto">

            <div class="m-1 col-12">
                <h2>Round {{$round->no}}</h2>
            </div>

            <div class="m-3 text-left col-12">
                <h2>Participating contestants</h2>
            </div>

            @foreach($contestants as $group => $contestant)
                <div class="col-lg-2">
                    <h5 class="pb-2">Group {{ $group }}</h5>
                    <div class="card shadow p-2">
                        <ul>
                            @foreach($contestants[$group] as $contestant)
                                <li class="text-left">{{ $contestant->name }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>

            @endforeach

            @if($waitingContestants->count() >0)
                <div class="m-3 pt-5 text-left col-12">
                    <h2>Waiting contestants</h2>
                </div>
                <div class="col-lg-2">
                    <div class="p-2 card shadow">
                        <ul>
                            @foreach($waitingContestants as $contestant)
                                <li class="text-left">{{ $contestant->name }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endif
            <div class="col-12 text-center m-2">
                <a id="next_round" href="{{ route('round.complete', $round->no) }}" class="btn btn-success"> Move to
                    next Round</a>
            </div>


        </div>
    </div>


@endsection
